with (import <nixpkgs> {});
let
    opkgs = import (builtins.fetchTarball {
        url = "https://github.com/NixOS/nixpkgs/archive/ee01de29d2f58d56b1be4ae24c24bd91c5380cea.tar.gz";
    }) {};
in mkShell {
  buildInputs = [
    opkgs.elmPackages.nodejs
    yarn
    lima
    colima
    docker-client
  ];
}
