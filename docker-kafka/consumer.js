const { Kafka } = require('kafkajs');

const kafka = new Kafka({
  clientId: 'test-play',
  brokers: ['localhost:9092'],
});

const consumer = kafka.consumer({ groupId: 'test-group' })

consumer.on(consumer.events.DISCONNECT, () => {
  console.log("CONSUMER-DISCONNECT");
});

const delay = (duration) =>
  new Promise(resolve => setTimeout(resolve, duration));

const run = async () => {
  await consumer.connect()
  await consumer.subscribe({ topic: 'test-topic', fromBeginning: true })

  await consumer.run({
    eachMessage: async ({ topic, partition, message }) => {
      await delay(3000);
      const date = new Date();

      console.log({
        pub: message.value.toString(),
        con: 'CON: ' + date.toISOString(),
      });
    },
  })
}

run().catch(e => console.error(`[CONSUMER] ${e.message}`, e))

const errorTypes = ['unhandledRejection', 'uncaughtException']
const signalTraps = ['SIGTERM', 'SIGINT', 'SIGUSR2']

errorTypes.forEach(type => {
  process.on(type, async e => {
    try {
      console.log(`process.on ${type}`)
      console.error(e)
      await consumer.disconnect()
      process.exit(0)
    } catch (_) {
      process.exit(1)
    }
  })
})

signalTraps.forEach(type => {
  process.once(type, async () => {
    try {
      await consumer.disconnect()
    } finally {
      process.kill(process.pid, type)
    }
  })
})
