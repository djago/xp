const { Kafka, Partitioners } = require('kafkajs');

(async () => {
  const kafka = new Kafka({
    clientId: 'test-play',
    brokers: ['localhost:9092'],
  });

  const producer = kafka.producer();

  await producer.connect();

  const date = new Date();
  await producer.send({
    topic: 'test-topic',
    messages: [
      { value: 'PUB: ' + date.toISOString() },
    ],
  });

  await producer.disconnect();
})();
